import express, { Express, Request, Response } from "express";
import { errorHandler } from "./middleware/errorHandler";
import channelRouter from "./routers/channel";
import messageRouter from "./routers/message";

const port = process.env.PORT;

const app: Express = express();

app.use("/channels", channelRouter);
app.use("/messages", messageRouter);
app.use(errorHandler);

app.get("/", (req: Request, res: Response) => {
  return res.status(200).send();
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
