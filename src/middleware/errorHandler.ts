import express, { Request, Response, NextFunction } from "express";
import { PrismaClient, Prisma } from "@prisma/client";

class ValidationError extends Error {
  constructor(message?: string) {
   super(message)
  }
}

const errorHandler = (err: Error, req: Request, res: Response, next: NextFunction) => {

  if (err instanceof ValidationError) {
    return res.status(400).send("ValidationError");
  }

  if (err instanceof Prisma.PrismaClientRustPanicError) {
    return res.status(400).send({
      type: err.name,
      details: err.message,
    });
  }

  if (err instanceof Prisma.PrismaClientUnknownRequestError) {
    return res.status(400).send({
      type: err.name,
      details: err.message,
    });
  }

  if (err instanceof Prisma.PrismaClientInitializationError) {
    return res.status(400).send({
      type: err.name,
      details: err.message,
    });
  }

  if (err instanceof Prisma.PrismaClientKnownRequestError) {
    return res.status(400).send({
      type: err.name,
      details: err.message,
    });
  }

  if (err instanceof Prisma.PrismaClientValidationError) {
    return res.status(400).send({
      type: err.name,
      details: err.message,
    });
  }
  return res.status(500).send("Something went wrong");
};

export { errorHandler, ValidationError };
