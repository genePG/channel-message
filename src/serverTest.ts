import express, { Express, Request, Response } from "express";
import { errorHandler } from "./middleware/errorHandler";
import channelRouter from "./routers/channel";
import messageRouter from "./routers/message";

const port = "9000";

const app: Express = express();

app.use("/channels", channelRouter);
app.use("/messages", messageRouter);
app.use(errorHandler);


export { app };
