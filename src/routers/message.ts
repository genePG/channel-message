import express, { NextFunction, Request, Response } from "express";
import { Prisma, PrismaClient } from "@prisma/client";
import { jsonParser } from "../utils";
import { errorHandler } from "../middleware/errorHandler";

const prisma = new PrismaClient();
const messageRouter = express.Router();

// message
// create
messageRouter.post("/", jsonParser, async (req: Request, res: Response, next: NextFunction) => {
  const { title, content, channelId } = req.body;
  try {
    const resp = await prisma.message.create({
      data: {
        title: title,
        content: content,
        channelId: channelId,
      },
    });
    res.send(resp);
  } catch (err) {
    next(err);
  }
});

// read
messageRouter.get("/", async (req: Request, res: Response, next: NextFunction) => {
  try {
    const messages = await prisma.message.findMany();
    res.send(messages);
  } catch (err) {
    next(err);
  }
});

// update
messageRouter.patch(
  "/:messageId",
  jsonParser,
  async (req: Request, res: Response, next: NextFunction) => {
    const { messageId } = req.params;
    const { title, content } = req.body;
    try {
      const resp = await prisma.message.update({
        where: { id: Number(messageId) || undefined },
        data: {
          title: title,
          content: content,
        },
      });
      res.send(resp);
    } catch (err) {
      next(err);
    }
  }
);

// delete
messageRouter.delete(
  "/:messageId",
  jsonParser,
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { messageId } = req.params;
      const resp = await prisma.message.delete({
        where: { id: Number(messageId) || undefined },
      });
      res.send(resp);
    } catch (err) {
      next(err);
    }
  }
);

export default messageRouter;
