import express, { Request, Response, NextFunction } from "express";
import { Prisma, PrismaClient } from "@prisma/client";
import { jsonParser } from "../utils";
import { ValidationError } from "../middleware/errorHandler";

const prisma = new PrismaClient();
const channelRouter = express.Router();

// create
channelRouter.post("/", jsonParser, async (req: Request, res: Response, next: NextFunction) => {
  const { name } = req.body;
  try {
    if (!name) {
      throw new ValidationError();
    }
    const resp = await prisma.channel.create({
      data: {
        name: name,
      },
    });
    res.send(resp);
  } catch (err) {
    next(err);
  }
});

// read
channelRouter.get("/", async (req: Request, res: Response, next: NextFunction) => {
  try {
    const channels = await prisma.channel.findMany();
    return res.send(channels);
  } catch (err) {
    next(err);
  }
});

channelRouter.get("/:channelId", async (req: Request, res: Response, next: NextFunction) => {
  const { channelId } = req.params;
  try {
    const channels = await prisma.channel.findUnique({
      where: {
        id: Number(channelId) || undefined,
      },
    });
    if (!channels) {
      throw new ValidationError(); 
    }
    return res.send(channels);
  } catch (err) {
    next(err);
  }
});

// update
channelRouter.patch(
  "/:channelId",
  jsonParser,
  async (req: Request, res: Response, next: NextFunction) => {
    const { channelId } = req.params;
    const { name } = req.body;
    try {
      const resp = await prisma.channel.update({
        where: { id: Number(channelId) || undefined },
        data: {
          name: name,
        },
      });
      return res.send(resp);
    } catch (err) {
      next(err);
    }
  }
);

// delete
channelRouter.delete(
  "/:channelId",
  jsonParser,
  async (req: Request, res: Response, next: NextFunction) => {
    const { channelId } = req.params;
    try {
      const resp = await prisma.channel.delete({
        where: { id: Number(channelId) || undefined },
      });
      return res.send(resp);
    } catch (err) {
      next(err);
    }
  }
);

export default channelRouter;
