import { describe, it, expect, beforeAll, afterAll } from '@jest/globals';
import supertest from "supertest";
import { Prisma, PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
import { app } from "../serverTest";

beforeAll(async () => {
  await prisma.channel.createMany({
    data: [{ id: 3, name: "test-channel-1" }, { id: 4, name: "test-channel-2" }, { id: 5, name: "test-channel-3" }],
  });
  console.log("✨ 3 channels successfully created!");

  await prisma.message.createMany({
    data: [
      {
        title: "title-1",
        content: "content-1",
        channelId: 3,
      },
      {
        title: "title-2",
        content: "content-2",
        channelId: 3,
      },
      {
        title: "title-3",
        content: "content-3",
        channelId: 3,
      },
    ],
  });

  console.log("✨ 2 messages successfully created!");
});

afterAll(async () => {
  const deleteMessage = prisma.message.deleteMany();
  const deleteChannel = prisma.channel.deleteMany();

  await prisma.$transaction([deleteMessage, deleteChannel]);
  await prisma.$disconnect();
  console.log('*** messages test after all')
});

it("should create 1 new message", async () => {
  const response = await supertest(app)
    .post("/messages")
    .set("Accept", "application/json")
    .send({
      title: "title-create",
      content: "content-create",
      channelId: 3,
    })

  expect(response.headers["content-type"]).toMatch(/json/);
  expect(response.status).toEqual(200);
});

it("should get messages", async () => {
  const response = await supertest(app).get("/messages/");
  expect(response.status).toEqual(200);
});

it("should update a messages", async () => {
  const response = await supertest(app).patch("/messages/3")
    .set("Accept", "application/json")
    .send({
      title: "title-change-3",
      content: "content-change-3",
    })
  expect(response.status).toEqual(200);
});

it("should delete messages", async () => {
  const response = await supertest(app).delete("/messages/3")
  expect(response.status).toEqual(200);
});

