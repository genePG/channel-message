import { describe, it, expect, beforeAll, afterAll } from "@jest/globals";
import { Prisma, PrismaClient } from "@prisma/client";
import supertest from "supertest";

import { app } from "../serverTest";

const prisma = new PrismaClient();

beforeAll(async () => {
  await prisma.channel.createMany({
    data: [{ name: "test-channel-1" }, { name: "test-channel-2" }],
  });
  console.log("*** 2 channels successfully created!");
});

afterAll(async () => {
  const messageChannel = prisma.message.deleteMany();
  const deleteChannel = prisma.channel.deleteMany();
  await prisma.$transaction([messageChannel, deleteChannel]);
  await prisma.$disconnect();

  console.log('*** channel test after all')
});

describe("channels", () => {
  it("create a channel", async () => {
    const response = await supertest(app)
      .post("/channels")
      .set("Accept", "application/json")
      .send({ name: "test-channel-1" });
    expect(response.headers["content-type"]).toMatch(/json/);
    expect(response.status).toEqual(200);
  });

  it("fail to create a channel", async () => {
    const response = await supertest(app)
      .post("/channels")
      .send()
      .set("Accept", "application/json");
    expect(response.status).toEqual(400);
  });

  it("get channels", async () => {
    await supertest(app).get("/channels").expect(200);
  });

  it("update a channel", async () => {
    await supertest(app).get("/channels/1").expect(200);
  });

  it("fail to update a channel", async () => {
    await supertest(app)
      .get("/channels/1000")
      .expect(400)
  });

  describe("update channel", () => {
    it("should return 200", async () => {
      const resp = await supertest(app).patch("/channels/1")
      expect(resp.status).toEqual(200) 
    });
    it("should return 400", async () => {
      const resp = await supertest(app).patch("/channels/1000")
      expect(resp.status).toEqual(400) 
    });
  });

  describe("delete channel", () => {
    it("should return 200", async () => {
      const response = await supertest(app).delete("/channels/1")
      expect(response.status).toEqual(200) 
    });
    it("should return 400", async () => {
      const response = await supertest(app).delete("/channels/1")
      expect(response.status).toEqual(400)
    });
  });
});
