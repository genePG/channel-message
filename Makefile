dc-start:
	docker compose up --build
dc-down:
	docker compose down
dc-restart:
	docker compose down
	docker compose up
dc-access-server:
	docker exec -it channel-message-server bash
dc-access-db:
	docker exec -it channel-message-db psql -U gene forum
